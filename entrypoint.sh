#!/usr/bin/env bash

# stop faircoind before container stops
stop_faircoind() {
  /home/faircoin/faircoin-cli stop > ./shutdown.log
}
trap 'stop_faircoind' SIGINT
trap 'stop_faircoind' SIGKILL
trap 'stop_faircoind' SIGTERM

while true; do : sleep 5 ; done
