# Pull base image.
FROM ubuntu:18.04

ENV LC_ALL=C.UTF-8 LANG=C.UTF-8

#####################################################################################root##
### install base libraries and dependencies for faircoin daemon ###########################
RUN apt-get update -q && \
    apt-get install -qy \
        net-tools \
        git \
        python3-pyqt5 \
        openssl \
        coreutils

#####################################################################################root##
### install nano commandline editor [optional] ( to edit faircoin.conf later if necessary )
RUN apt-get install -qy nano

#####################################################################################root##
### install python packages
RUN apt-get install -qy \
        python3-pip \
        libleveldb-dev

RUN python3 -m pip install \
        aiorpcx \
        attrs \
        plyvel \
        pylru \
        aiohttp

#####################################################################################root##
### system cleanup ########################################################################
RUN rm -rf /var/lib/apt/lists/* && \
    apt-get autoremove -y && \
    apt-get clean

#####################################################################################root##
### create and run user account to image ##################################################
ARG RUNNER_GID
ARG RUNNER_UID
ENV USER=faircoin
ENV FAIRCHAINS_PATH=/home/faircoin/.fairchains/
RUN groupadd -g $RUNNER_GID faircoin
RUN useradd --create-home --shell /bin/bash faircoin --uid $RUNNER_UID --gid $RUNNER_GID

RUN mkdir -p /home/faircoin/.fairchains
RUN chown -R faircoin:faircoin /home/faircoin/.fairchains

RUN mkdir -p /home/faircoin/scripts
RUN chown -R faircoin:faircoin /home/faircoin/scripts

USER faircoin
#################################################################################faircoin##
### download electrumfairchainsx & build ##################################################
WORKDIR /home/faircoin
RUN git clone https://github.com/fairchainsx/electrumfairchainsx.git

WORKDIR /home/faircoin/electrumfairchainsx
RUN python3 setup.py build
